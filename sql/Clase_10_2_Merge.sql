USE PlatziSQL

SELECT * FROM PlatziSQL.dbo.UsuarioTarget;
SELECT * FROM PlatziSQL.dbo.UsuarioSource;
GO

CREATE PROCEDURE MergeUsuarioTarget
@codigo integer,
@nombre varchar(100),
@puntos integer
AS
BEGIN

	MERGE UsuarioTarget T
	USING (SELECT @codigo, @nombre, @puntos) AS S
			(codigo, nombre, puntos)
		ON (T.CODIGO = S.CODIGO)
	WHEN MATCHED THEN
		UPDATE SET T.NOMBRE = S.NOMBRE,
					T.PUNTOS = S.PUNTOS
	WHEN NOT MATCHED THEN
		INSERT (CODIGO, NOMBRE, PUNTOS)
		VALUES (S.CODIGO, S.NOMBRE, S.PUNTOS);
END;
GO

-- Ejecutar el procedimiento almacenado
SELECT * FROM UsuarioTarget
exec MergeUsuarioTarget 3, 'Pablo Huete', 9

-- Se verifican los datos
SELECT * FROM UsuarioTarget