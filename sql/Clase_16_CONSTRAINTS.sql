USE PlatziSQL
GO
SELECT * FROM UsuarioSource;
GO

ALTER TABLE UsuarioSource ADD CONSTRAINT c_UsuarioSource_Puntos DEFAULT((0)) FOR Puntos;

INSERT INTO UsuarioSource
VALUES (7, 'Carlos Alvarez', 10)
GO

DELETE UsuarioSource WHERE Codigo = 7;

ALTER TABLE UsuarioSource ADD CONSTRAINT c_UsuarioSource_Nombre UNIQUE (Nombre);
GO

ALTER TABLE UsuarioSource ADD CONSTRAINT c_UsuarioSource_Valida CHECK(Puntos > 0 AND Nombre <> 'Maria Solis')
GO

INSERT INTO UsuarioSource
VALUES (9, 'Maria Solis', -1)