USE PlatziSQL
GO
-- TRIGGERS ADMINISTRATIVOS

-- TRIGGER	que valida que no se puedan borrar tablas o cambiar estructura de tablas
CREATE OR ALTER TRIGGER t_valida_tablas
ON DATABASE
FOR DROP_TABLE, ALTER_TABLE AS

	PRINT 'No es permitido modificar la estructura de la tabla, comuniquese con el DBA';
	ROLLBACK;
GO

ALTER TABLE UsuarioTarget
ALTER COLUMN Nombre VARCHAR(200);

-- Borrado el TRIGGER para seguir
--DROP TRIGGER t_valida_tablas;

GO
-- TRIGGER	que valida que no se puedan crear bases de datos en el servidor
CREATE OR ALTER TRIGGER t_valida_base_de_datos
ON ALL SERVER
FOR CREATE_DATABASE AS
	
	PRINT 'No se puede crear la base de datos';
	ROLLBACK;