USE PlatziSQL
GO
SELECT * FROM UsuarioTarget
SELECT * FROM UsuarioSource
GO

CREATE OR ALTER TRIGGER t_insert_usario_target
ON UsuarioTarget
AFTER INSERT AS
BEGIN

	IF (ROWCOUNT_BIG() = 0)
		RETURN;

	SELECT Codigo, Nombre, Puntos
	FROM INSERTED

	Print 'Se realizo el INSERT'
END;

insert into UsuarioTarget (Codigo, Nombre, Puntos)
VALUES (7, 'Carlos Soto', 5);
GO

CREATE OR ALTER TRIGGER t_update_usario_target
ON UsuarioTarget
AFTER UPDATE AS
BEGIN

	IF (ROWCOUNT_BIG() = 0)
		RETURN;

	SELECT Codigo, Nombre, Puntos
	FROM INSERTED

	Print 'Se realizo el UPDATE'
END;


UPDATE UsuarioTarget
SET Nombre = 'Carlos Ramirez'
WHERE Codigo = 7;
GO

CREATE OR ALTER TRIGGER t_delete_usario_target
ON UsuarioTarget
AFTER DELETE AS
BEGIN

	IF (ROWCOUNT_BIG() = 0)
		RETURN;

	SELECT Codigo, Nombre, Puntos
	FROM DELETED

	Print 'Se realizo el DELETE'
END;

DELETE UsuarioTarget
WHERE Codigo = 7;
