USE PlatziSQL
GO

SELECT * FROM UsuarioSource

-- Para cerar un Full Text Catalog debemos ir a la base de datos (PlatziSQL), STORAGE y dar click derecho en Full Text Catalog
-- y seleccionar New Full Text Catalog y la seccion de tablas o vistas seleccionamos la tabla y agregamos la columna que pertence al catalogo

SELECT FULLTEXTSERVICEPROPERTY('IsFulltextInstalled'); -- verificar si el servicio de busqueda esta instalado

--> Luego de crear el catalogo

SELECT * FROM UsuarioSource
WHERE CONTAINS (Nombre, 'Marco');  --> Busqueda dentro del catalogo

SELECT * FROM UsuarioSource
WHERE FREETEXT(Nombre, 'Castro Alberto');
--> Devuelve todos los que cumplen con la busqueda de todos los que cumplen el criterio sin importar el orden
Codigo	Nombre	Puntos
4	Alberto Ruiz Castro	4
5	Alejandro Castro	5

DROP TABLE [dbo].[Documentos];

CREATE TABLE [dbo].[Documentos](
	[id] [int] NOT NULL,
	[NombreArchivo] [nvarchar](40) NULL,
	[Contenido] [varbinary](max) NULL,
	[extension] [varchar](5) NULL,
 CONSTRAINT [PK_Documentos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
-- Pasar los archivos al servidor utilizando el comando scp o Visual Studio Code
/*
INSERT INTO Documentos
SELECT 1,N'Prueba-01', BulkColumn,'.doc'
FROM OPENROWSET(BULK  N'/root/temp/1.doc', SINGLE_BLOB) blob;

INSERT INTO Documentos
SELECT 2,N'Prueba-02', BulkColumn,'.doc'
FROM OPENROWSET(BULK  N'/root/temp/2.doc', SINGLE_BLOB) blob;

INSERT INTO Documentos
SELECT 3,N'Prueba-03', BulkColumn,'.doc'
FROM OPENROWSET(BULK  N'/root/temp/3.doc', SINGLE_BLOB) blob;

INSERT INTO Documentos
SELECT 4,N'Prueba-04', BulkColumn,'.doc'
FROM OPENROWSET(BULK  N'/root/temp/4.doc', SINGLE_BLOB) blob;
*/

sp_help '[Documentos]'

SELECT * FROM [Documentos];


SELECT * FROM Documentos
WHERE FREETEXT (Contenido, 'Pablo');
GO

SELECT * FROM Documentos
WHERE FREETEXT (Contenido, 'Alberto');
GO
------------------------------------------------

USE AdventureWorks2022

SELECT Name, ListPrice  
FROM Production.Product  
WHERE CONTAINS(Name, 'Mountain') 

SELECT Title, *  
FROM Production.Document  
WHERE FREETEXT (Document, 'important bycycle guidelines')  

SELECT Title  
FROM Production.Document  
WHERE FREETEXT (Document, 'vital safety components') 


select * from Production.ProductDescription 
where CONTAINS(Description, 'NEAR((lightweight, aluminum), 10)')


-- haciendo join con el catalogo
SELECT KEY_TBL.RANK, FT_TBL.Description  
FROM Production.ProductDescription AS FT_TBL   
     INNER JOIN  
     FREETEXTTABLE(Production.ProductDescription, Description,  
                    'perfect all-around bike') AS KEY_TBL  
     ON FT_TBL.ProductDescriptionID = KEY_TBL.[KEY]  
WHERE KEY_TBL.RANK >= 10  
ORDER BY KEY_TBL.RANK DESC  