USE PlatziSQL
GO
SELECT * FROM UsuarioTarget
SELECT * FROM UsuarioSource
GO

CREATE OR ALTER TRIGGER t_insert_usario_target
ON UsuarioTarget
AFTER INSERT AS
BEGIN

	IF (ROWCOUNT_BIG() = 0)
		RETURN;
	
	DECLARE @codigo int;
		SELECT @codigo = Codigo FROM INSERTED;

	BEGIN
		IF @codigo > 10
			PRINT 'No se realizo el INSERT';
			ROLLBACK;
			RETURN;
	END;
	--SELECT Codigo, Nombre, Puntos
	--FROM INSERTED

	Print 'Se realizo el INSERT'
END;

insert into UsuarioTarget (Codigo, Nombre, Puntos)
VALUES (11, 'Juan Carlos', 5);
GO

CREATE OR ALTER TRIGGER t_update_usario_target
ON UsuarioTarget
AFTER UPDATE AS
BEGIN

	IF (ROWCOUNT_BIG() = 0)
		RETURN;

	DECLARE
		@puntos int;
	BEGIN
		SELECT @puntos = Puntos FROM INSERTED;
		IF @puntos < 0
			PRINT 'Los puntos no pueden ser menor que 0, No se realizo el UPDATE'
			ROLLBACK;
			RETURN;
	END;
	--SELECT Codigo, Nombre, Puntos
	--FROM INSERTED

	Print 'Se realizo el UPDATE'
END;


UPDATE UsuarioTarget
SET Puntos = -1
WHERE Codigo = 3;

GO

CREATE OR ALTER TRIGGER t_delete_usario_target
ON UsuarioTarget
AFTER DELETE AS
BEGIN

	IF (ROWCOUNT_BIG() = 0)
		RETURN;

	SELECT Codigo, Nombre, Puntos
	FROM DELETED

	Print 'Se realizo el DELETE'
END;

DELETE UsuarioTarget
WHERE Codigo = 7;
