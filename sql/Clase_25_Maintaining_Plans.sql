
-- Consulta para conocer fragmentación de indices tablas en base de datos seleccionada

SELECT  OBJECT_NAME(IDX.OBJECT_ID) AS Table_Name, 
IDX.name AS Index_Name, 
IDXPS.index_type_desc AS Index_Type, 
IDXPS.avg_fragmentation_in_percent  Fragmentation_Percentage
FROM sys.dm_db_index_physical_stats(DB_ID(), NULL, NULL, NULL, NULL) IDXPS 
INNER JOIN sys.indexes IDX  ON IDX.object_id = IDXPS.object_id 
AND IDX.index_id = IDXPS.index_id 
ORDER BY Fragmentation_Percentage DESC


sp_configure 'show advanced options', 1;  
GO  
RECONFIGURE;  
GO  
sp_configure 'Agent XPs', 1;  
GO  
RECONFIGURE  
GO

-- Crear un plan de mantenimiento

USE [PlatziSQL];
GO
--  Adds a new job, executed by the SQL Server Agent service, called "HistoryCleanupTask_1".
EXEC [dbo].[sp_add_job] @job_name = N'HistoryCleanupTask_1',
                        @enabled = 1,
                        @description = N'Clean up old task history';
GO
-- Adds a job step for reorganizing all of the indexes in the HumanResources.Employee table to the HistoryCleanupTask_1 job.
EXEC [dbo].[sp_add_jobstep] @job_name = N'HistoryCleanupTask_1',
                            @step_name = N'Reorganize all indexes on HumanResources.Employee table',
                            @subsystem = N'TSQL',
                            @command = N'USE [AdventureWorks2022];
GO
ALTER INDEX [AK_Employee_LoginID]
ON [HumanResources].[Employee]
REORGANIZE
WITH (LOB_COMPACTION = ON);
GO
USE [AdventureWorks2022];
GO
ALTER INDEX [AK_Employee_NationalIDNumber]
ON [HumanResources].[Employee]
REORGANIZE
WITH (LOB_COMPACTION = ON);
GO
USE [AdventureWorks2022];
GO
ALTER INDEX [AK_Employee_rowguid]
ON [HumanResources].[Employee]
REORGANIZE
WITH (LOB_COMPACTION = ON);
GO
USE [AdventureWorks2022];
GO
ALTER INDEX [IX_Employee_OrganizationLevel_OrganizationNode]
ON [HumanResources].[Employee]
REORGANIZE
WITH (LOB_COMPACTION = ON);
GO
USE [AdventureWorks2022];
GO
ALTER INDEX [IX_Employee_OrganizationNode]
ON [HumanResources].[Employee]
REORGANIZE
WITH (LOB_COMPACTION = ON);
GO
USE [AdventureWorks2022];
GO
ALTER INDEX [PK_Employee_BusinessEntityID]
ON [HumanResources].[Employee]
REORGANIZE
WITH (LOB_COMPACTION = ON);
GO',
                            @retry_attempts = 5,
                            @retry_interval = 5;
GO
-- Creates a schedule named RunOnce that executes every day when the time on the server is 23:30.
EXEC [dbo].[sp_add_schedule] @schedule_name = N'RunOnce',
                             @freq_type = 4,
                             @freq_interval = 1,
                             @active_start_time = 233000;
GO
-- Attaches the RunOnce schedule to the job HistoryCleanupTask_1.
EXEC [dbo].[sp_attach_schedule] @job_name = N'HistoryCleanupTask_1',
                                @schedule_name = N'RunOnce';
GO