-- Curso Optimización SQL SERVER
-- Roy Rojas
-- twitter.com/royrojasdev | linkedin.com/in/royrojas
------------------------------------------------------
-- Clase 23 - Tablas Temporales y Tablas Variables
------------------------------------------------------


USE PlatziSQL

SELECT * FROM UsuarioSource

CREATE TABLE #UsuarioSource
(Codigo int, Nombre varchar(100))

INSERT INTO #UsuarioSource
SELECT Codigo, Nombre 
  FROM UsuarioSource
 WHERE Codigo < 4
 
SELECT * FROM #UsuarioSource

-- Estas tablas se eliminan cuando se muere la sesion,
-- Se recomienda eliminar la tabla temporal cuando se utilizan en un procedimiento almacenado

DROP TABLE #UsuarioSource

-- Tabla GLOBAL ##
-- Tablas las puede ver cualquier usuario en SQL
--- Recomendacion no usarlas
SELECT * FROM ##UsuarioSource

EXEC msp_prueba

CREATE PROCEDURE msp_prueba
AS 
BEGIN
	SELECT * FROM #UsuarioSource
END


DROP TABLE #UsuarioSource
DROP PROCEDURE msp_prueba


-----
-- Tablas variales

DECLARE @VariableTabla 
TABLE (Codigo int, Nombre varchar(100))

INSERT INTO @VariableTabla
SELECT Codigo, Nombre 
  FROM UsuarioSource
 WHERE Codigo < 4

SELECT * FROM @VariableTabla