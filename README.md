# Configurar un servidor SQL Server con DEBIAN, DOCKER y BASH

## Utilizar la linea de comandos de Docker sin privilegios *SUDO* en LINUX

Visitar [Manage Docker as a non-root user](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user)

## Instalacion

### 1. Extraiga la imagen de contenedor de Linux de SQL Server 2022 (16.x) desde Microsoft Container Registry

```bash
docker pull mcr.microsoft.com/mssql/server:2022-latest
```

### 2. Para ejecutar la imagen de contenedor de Linux con Docker, puede usar el siguiente comando desde un shell de Bash, o bien una línea de comandos de PowerShell con privilegios elevados (forma sencilla)

```bash
docker run -e "ACCEPT_EULA=Y" -e "MSSQL_SA_PASSWORD=<YourStrong@Password>"  \
   -p 1433:1433 --name sqlserver-fts --hostname sqlserver-fts \
   -d mcr.microsoft.com/mssql/server:2022-latest
```

#### Crear la maquina con un archivo *compose.yaml*  (remendado)

Cree un archivo en la carpeta de trabajo y utilice el nombre *compose.yaml*

```yaml
version: "3.2"
services:

  sqlserver:
    container_name: sqlserver-fts
    hostname: mssqlfts
    build:
      dockerfile: Dockerfile
    ports:
      - "1434:1433"
    environment:
      MSSQL_SA_PASSWORD: "YourStrong@Password"
      ACCEPT_EULA: "Y"
      MSSQL_AGENT_ENABLED: true
```

Cree un archivo *Dockerfile* con este contenido [Dockerfile](./Dockerfile)

### 3. Para ver los contenedores de Docker, use el comando ***docker ps***

```bash
docker ps -a
```

### 4. Para ver errores, aunque el status de la imagen sea ***up***

```bash
docker exec -t sqlserver-fts cat /var/opt/mssql/log/errorlog | grep connection
```

## Conectar a SQL Server

### 1. Use el comando docker exec -it para iniciar un shell de Bash interactivo dentro de su contenedor en ejecución. En el ejemplo siguiente, el parámetro --name especifica el nombre de *sqlserver-fts* al crear el contenedor

```bash
docker exec -it sqlserver-fts "bash"
```

### 2. Una vez dentro del contenedor, conéctese localmente con sqlcmd mediante su ruta de acceso completa

```bash
/opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P "<YourStrong@Password>"
```

### 3. Si se realiza correctamente, debe ver un símbolo de sistema de sqlcmd: ***1>***

## Creación y consulta de datos

### Creación de una base de datos

#### 1. En el símbolo del sistema de sqlcmd, pegue el comando Transact-SQL siguiente para crear una base de datos de prueba

```SQL
CREATE DATABASE TestDB;
```

#### 2. En la línea siguiente, escriba una consulta para devolver el nombre de todas las bases de datos del servidor

```SQL
SELECT Name from sys.databases;
```

#### 3. Los dos comandos anteriores no se ejecutaron de inmediato. Escriba GO en una línea nueva para ejecutar los comandos anteriores

```SQL
GO
```

### Insertar Datos

Luego cree una tabla, Inventory, e inserte dos filas nuevas.

#### 1. En el símbolo del sistema de ***sqlcmd***, cambie el contexto a la nueva base de datos ```TestDB```

```SQL
USE TestDB;
```

#### 2. Cree una tabla llamada ```Inventory```

```SQL
CREATE TABLE Inventory (id INT, name NVARCHAR(50), quantity INT);
```

#### 3. Inserte datos en la nueva tabla

```SQL
INSERT INTO Inventory VALUES (1, 'banana', 150);
INSERT INTO Inventory VALUES (2, 'orange', 154);
```

#### 4. Escriba GO para ejecutar los comandos anteriores

```SQL
GO
```

### Salida del símbolo del sistema de sqlcmd

#### 1. Para finalizar la sesión de **sqlcmd**, escriba ```QUIT```

```bash
QUIT
```

#### 2. Para salir de la línea de comandos interactiva del contenedor, escriba exit. El contenedor continuará ejecutándose después de salir del shell de Bash interactivo

## Bases de datos de pruebas

### [Descargar AdventureWorks](https://docs.microsoft.com/en-us/sql/samples/adventureworks-install-configure?view=sql-server-ver15&tabs=ssms)

### [Descargar Wide World Importers](https://github.com/Microsoft/sql-server-samples/releases/tag/wide-world-importers-v1.0)

## Restaurar un back up a SQL Server

### Puede usar el archivo .bak para restaurar su base de datos de muestra en su instancia de SQL Server. Puede hacerlo con el comando RESTORE (Transact-SQL) o con la interfaz gráfica (GUI) en SQL Server Management Studio (SSMS) o Azure Data Studio

#### Windows restore

Para restaurar ***AdventureWorks2022*** en Windows, modifique los valores según corresponda a su entorno y luego ejecute el siguiente comando Transact-SQL (T-SQL)

```bash
USE [master];
GO
RESTORE DATABASE [AdventureWorks2022]
FROM DISK = N'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\Backup\AdventureWorks2022.bak'
WITH
    FILE = 1,
    NOUNLOAD,
    STATS = 5;
GO
```

#### Linux con Docker

a. Ingresar al bash de la maquina docker y crear la carpeta backup primero

```console
docker exec -it mkdir /var/opt/mssql/backup
```

b. COPIAR los archivos de respaldo de la maquina debian a la maquina docker

```bash
docker cp ./AdventureWorks2022.bak sqlserver-fts:/var/opt/mssql/backup/
docker cp ./WideWorldImporters-Full.bak sqlserver-fts:/var/opt/mssql/backup/
```

c. Para restaurar ***AdventureWorks2022*** en Linux, cambie la ruta del sistema de archivos de Windows a Linux y luego ejecute el siguiente comando Transact-SQL (T-SQL)

***AdventureWorks2022***

```SQL
USE master;
GO
RESTORE DATABASE [AdventureWorks2022]
FROM DISK = '/var/opt/mssql/backup/AdventureWorks2022.bak'
WITH
    MOVE 'AdventureWorks2022' TO '/var/opt/mssql/data/AdventureWorks2022.mdf',
    MOVE 'AdventureWorks2022_log' TO '/var/opt/mssql/data/AdventureWorks2022_log.ldf',
    FILE = 1,
    NOUNLOAD,
    STATS = 5;
GO
```

***WideWorldImporters***

```SQL
RESTORE DATABASE WideWorldImporters
FROM DISK = '/var/opt/mssql/backup/WideWorldImporters-Full.bak' WITH
MOVE 'WWI_Primary' TO '/var/opt/mssql/data/WideWorldImporters.mdf',
MOVE 'WWI_UserData' TO '/var/opt/mssql/data/WideWorldImporters_UserData.ndf',
MOVE 'WWI_Log' TO '/var/opt/mssql/data/WideWorldImporters_log.ldf',
MOVE 'WWI_InMemory_Data_1' TO '/var/opt/mssql/data/WideWorldImporters_InMemory_Data_1',
FILE = 1,
NOUNLOAD,
STATS = 5;
GO
```

NOTA: se deben mover **(MOVE)** los archivos del backup al sistema de archivos de la maquina, para ver los nombres se utiliza el comando

```SQL
RESTORE FILELISTONLY FROM DISK = '/var/opt/mssql/backup/<NOMBRE_ARCHIVO.bak>'
```

### Crear un usuario y login en SQL Server

Crear usuario

```sql
CREATE USER pablo_platzi FROM LOGIN pablo_platzi;
GO
```

Permisos para conectarse a cualquier BD (analizar si es necesario)

```sql
GRANT CONNECT ANY DATABASE TO pablo_platzi;
GO
```

Otorga control total sobre el servidor (usar con precaucion)

```sql
GRANT CONTROL SERVER TO pablo_platzi;
GO
```

---

Nota: Reiniciar el servidor, y Desconectarse y volverse a conectar para restablecer los permisos del usuario

---

## Links de utilidad

[Inicio rápido: Ejecución de imágenes de contenedor de SQL Server para Linux con Docker](https://learn.microsoft.com/es-es/sql/linux/quickstart-install-connect-docker?view=sql-server-ver16&pivots=cs1-bash)

[Planes de ejecución de consultas – Entendiendo y leyendo los planes](https://www.sqlshack.com/es/planes-de-ejecucion-de-consultas-entendiendo-y-leyendo-los-planes/)

## Configuración de un Database Mail

El servicio de correos nos va a ayudar principalmente para notificaciones por eventos generados en nuestro servidor, ya sea para reportar sobre errores, procesos como backups, algún procedimiento almacenado ejecutado o cualquier proceso que queramos tener controlado. En realidad es muy útil, yo lo utilizo a diario.

### Configuración

![alt](./imgs/img_1.png)

1. Creación del perfil. En el asistente buscamos Database Mail y seleccionamos Configurar. En la siguiente pantalla nos va a indicar que debemos crear el perfil inicial.

    ![alt](./imgs/img_2.png)

    En la siguiente pantalla nos va a preguntar si queremos habilitar el servicio de correo, le indicamos que sí y continuamos.

    ![alt](./imgs/img_3.png)

    ![alt](./imgs/img_4.png)

    Comenzamos a llenar los datos para el perfil, y seleccionamos Add, para configurar la cuenta de correo asociada.

    ![alt](./imgs/img_5.png)

    Llenamos todos los campos requeridos y seleccionamos OK. Cuando se cierra la pantalla de la cuenta de correo, continuamos con el proceso, seleccionamos Next en las siguientes tres pantallas.

    ![alt](./imgs/img_6.png)

    ![alt](./imgs/img_7.png)

    ![alt](./imgs/img_8.png)

    ![alt](./imgs/img_9.png)

2. Ahora que tenemos el perfil creado, continuamos con la creación del correo electrónico.

    ```sql
    sp_configure 'show advanced', 1
    GO
    RECONFIGURE
    GO
    sp_configure 'Database Mail XPs', 1
    GO
    RECONFIGURE
    GO
    ```

    Ejecutamos el query para configurar el servicio

    ![alt](./imgs/img_10.png)

3. En el último paso probamos el servicio y enviamos un correo

```sql
USE msdb
GO
EXEC sp_send_dbmail @profile_name='David Huete',
@recipients='[micorreo@gmail.com](mailto:micorreo@gmail.com)',
@subject='Mensaje de prueba',
@body='Felicidades ya puedes enviar correos
desde tu base de datos'
```

![alt](./imgs/img_11.png)

Felicidades, ya tienes configurado tu servidor de correos en SQL Server

## Configurando TempDB

La base de datos TempDB en realidad debería de llamarse ImportanDB. En todos los años que tengo brindando consultorías de SQL este es uno de los problemas más recurrentes, la mala configuración de esta base de datos.

### Recomendaciones

1. Separación de los archivos de datos de la base de datos TempDB en otro disco. Es muy importante para efectos de rendimiento y de monitoreo del tamaño de esta base de datos en un disco propio, separado del disco donde tenemos los archivos de datos de nuestras bases de datos o sistema operativo. Es importante para la separación de las cargas de trabajo. Tomando como base esta recomendación, es importante que puedas tener tus archivos de datos de las bases de datos propias en un disco, y otro disco para los log. Entonces vas a ocupar mínimo tres discos en producción 1. TempDB, 2. Archivos mdf o ndf, 3. Archivos log.

    Si ya tienes tu servidor en producción y quieres mover la base de datos TempDB a otro disco, te adjunto el script para realizarlo:

    ```sql
    USE master; 
    GO 
    ALTER DATABASE tempdb
    MODIFY FILE (NAME = tempdev, FILENAME = '/var/opt/mssql/data/tempdb.mdf');
    GO 
    ALTER DATABASE tempdb
    MODIFY FILE (NAME = templog, FILENAME = '/var/opt/mssql/data/templog.ldf');
    GO
    -- Después de esto es requerido reiniciar la base de datos
    ```

2. Tamaño inicial y crecimiento automático. En una instalación inicial en versiones previas a la 2019, es probable que se creará una base de datos TempBD con un archivo inicial de 8MB y un archivos log de 1MB y con un crecimiento automático de 10%. Es importante recalcar que después del uso, la base de datos va a ir creciendo y en el momento que el servidor se reinicie por cualquier motivo, TempDB volverá al tamaño de la configuración base.
¿De qué tamaño tengo que configurar mi TempDB? Esta es una pregunta muy difícil, va a depender de la carga de tu servidor. Si tienes la opción de tener un disco propio para esta importante base de datos y tienes la posibilidad de asignarle bastantes gigas, entonces puedes indicarle que el tamaño original sea de un 90% de este disco, así te puedes asegurar que ningún otro archivo va a usar ese disco y no te vas a preocupar por el crecimiento automático. Si no puedes tener tu propio disco, vas a tener que administrar el tamaño. En este caso puedes dejar por un tiempo prudencial este crezca y así sabrás cuál va a ser el comportamiento que va a tener antes de asignarle un tamaño inicial y asignarle un crecimiento automático considerable, para que no este proceso no se ejecute de forma regular.

    ¿Cuál es el crecimiento automático recomendable? Si tienes un disco propio y pudiste separar el espacio, no vas a tener que configurar este parámetro ya que no se va a usar en el mejor de los casos. Puedes dejarlo configurado a una cantidad de megas considerable, pensando en el espacio en disco libre. Si no tienes el disco propio y compartes unidad con el sistema operativo, por ejemplo, entonces después de un tiempo prudencial viendo el comportamiento de esta base de datos, ya sabrás cuál valor para el crecimiento automático pueda tener, por ejemplo, si viste que todos los días la BD crece 5 megas, entonces puedes asignarle un valor de crecimiento de 50MB, con esto vas a asegurarte que no todos los días se ejecutará el autoincremento, no vas a tener un cuello de botella y así administrar mejor los recursos del servidor.

3. Tener varios archivos para la base de datos. Si tenemos la posibilidad de crear varios archivos. vamos a aumentar el rendimiento en E/S en nuestro servidor. SQL va a utilizar un algoritmo de llenado proporcional y con esto nos aseguramos que vamos a tener menos bloqueos y menos cuellos de botella, ya que si un archivo está haciendo algún proceso importante, SQL va a utilizar otro disponible.
¿Cuántos archivos? Antes se recomendaba que fuera un archivo por CPU, que aunque todavía es un buen comienzo ya no es necesario, ahora se recomienda que la configuración inicial comenzar con 8 archivos y conforme se va analizando el comportamiento añadir archivos adicionales si es necesario

![alt](./imgs/img_12.png)

## Monitoreo con sp_who3

Uno de los procedimientos almacenados que más utilizo en mi día a día es sp_who3. Con este procedimiento se puede ver qué sucede en tiempo real en mi servidor de base de datos. El query de creación puedes encontrarlo aquí.

Es importante que a la hora de crearlo lo hagamos en la base de datos master, así desde cualquier base de datos lo podremos ejecutar. Importante aclarar que cualquier SP que hagamos en la base de datos master se puede utilizar desde cualquier lugar de nuestro servidor de base de datos. Por este motivo aquí es que vamos a crear los procedimientos de monitoreo.

Al ejecutar el procedimiento nos va a retornar varios datos importantes, como el número de sesión, cuál usuario está logueado, desde cuál computadora, si existe un bloqueo nos va a indicar cuál sesión es la que lo está realizando y así poder tomar medidas. También nos va a mostrar la cantidad de milisegundos que lleva ejecutándose un query, consumo de CPU, IO y otros datos más que conocerás conforme utilices el procedimiento.

![alt](./imgs/img_13.png)

## Monitoreo de actividades

En esta lectura separamos el monitoreo de actividades de la lectura anterior de sp_who3, de otros queries, principalmente por la importancia del query anterior. El query de esta clase puedes encontrarlo en [https://github.com/royrojas/Platzi-SQL-Optimizacion/tree/29-query-monitoreo-actividades](https://github.com/royrojas/Platzi-SQL-Optimizacion/tree/29-query-monitoreo-actividades)

Estos son solo unos que utilizo mucho en mi día a día como DBA y en el camino te aseguro que vas a ir modificando estos y creando tus propios queries, vas a ir aprendiendo a facilitarte el trabajo con querys que vas a ir guardando poco a poco. Te recomiendo utilizar los Gists de GitHub para tener ordenados tus scripts.

### Consulta a los objetos de la base de datos

Cuando consultamos la tabla sys.objects vamos a poder obtener información sobre todos los objetos que hemos creado en nuestra base de datos. Cada objeto tiene un id para poder identificarlo y un tipo de objeto (tabla de usuario, tabla de sistema, procedimiento almacenado, primary key, función, vista, etc.) Con estos primeros queries vamos a poder ver la fecha de creación y la fecha en que se modificó el objeto

```sql
SELECT name
FROM sys.objects
WHERE type = 'P'
AND DATEDIFF(D,modify_date, GETDATE()) < 7
```

Por ejemplo, en este query vemos los objetos tipo P (procedimiento almacenado) que se modificaron en los últimos 7 días.

En otro ejemplo podemos ver los triggers creados o modificados

```sql
SELECT
o.name as [Trigger Name],
CASE WHEN o.type = 'TR' THEN 'SQL DML Trigger'
     WHEN o.type = 'TA' THEN 'DML Assembly Trigger' END
     AS [Trigger Type],
sc.name AS [Schema_Name],
OBJECT_NAME(parent_object_id) as [Table Name],
o.create_date [Trigger Create Date], 
o.modify_date [Trigger Modified Date] 
FROM sys.objects o
INNER JOIN sys.schemas sc ON o.schema_id = sc.schema_id
WHERE (type = 'TR' OR type = 'TA')
AND ( DATEDIFF(D,create_date, GETDATE()) < 7 OR
    DATEDIFF(D,modify_date, GETDATE()) < 7) -- Last 7 days
```

Como puedes ver en el ejemplo, modificar el query para tus propios requerimientos es muy sencillo

### Actualización rápida de índices

Si quieres actualizar todos los índices de tu base de datos de una forma muy rápida esta puede ser una opción. Aunque siempre te voy a recomendar hacerlo en planes de mantenimiento, esta opción nos puede ayudar principalmente en bases de datos pequeñas y también en servidores con SQL Express

```sql
EXEC sp_MSforeachtable @command1="print '?' DBCC DBREINDEX ('?', ' ', 80)"
GO
EXEC sp_updatestats
```

En este caso el factor de relleno está en 80, el cual es el porcentaje de espacio en cada página de índice para almacenar datos cuando se crea o reconstruye el índice. Si indicamos valor 0 se va a usar el último valor utilizado o el valor configurado como valor por defecto.

### Estado del avance de procesos

Otro query que utilizo regularmente es el de avance en procesos.

```sql
-- ver avances de procesos
SELECT session_id as SPID, command, a.text AS Query, start_time, 
percent_complete, dateadd(second,estimated_completion_time/1000, 
getdate()) as estimated_completion_time 
FROM sys.dm_exec_requests r CROSS APPLY 
     sys.dm_exec_sql_text(r.sql_handle) a 
WHERE r.command in ('BACKUP DATABASE','RESTORE DATABASE',
                    'BACKUP LOG','DbccFilesCompact',
                    'DbccSpaceReclaim','DBCC')
```

En este ejemplo podemos ver el avance en procesos como backups, procesos de mantenimiento como DBCC y restaurar base de datos (‘BACKUP DATABASE’,‘RESTORE DATABASE’, ‘BACKUP LOG’,‘DbccFilesCompact’, ‘DbccSpaceReclaim’,‘DBCC’). Pero estos valores los puedes modificar por otros más, si ejecutamos el query sin where podremos ver más opciones y dependiendo de nuestras necesidades podremos consultar sobre procesos de reconstrucción de índices en bases de datos pesadas. Se usa para ver actividades que pueden durar muchos minutos ya que nos muestra el tiempo transcurrido y un estimado del tiempo pendiente para finalizar un proceso

### Monitoreo del tamaño de la base de datos

Otros queries útiles son para monitorear el tamaño de las bases de datos

![alt](./imgs/img_14.png)

Si ejecutamos el sp_spaceused nos va mostrar el tamaño de la base de datos donde tenemos la sesión actual, se puede utilizar de esta forma o también podríamos incluirlo en algún JOB y que nos alerte eventualmente si llega a algún tamaño.

Una mejor forma de poder obtener el tamaño de nuestras bases de datos y que nos puede mostrar información más útil es el siguiente query:

```sql
select db_name() as dbname,
name as filename,
size/128.0 as currentsize,
size/128.0 - cast(fileproperty(name,'SpaceUsed') as int)/128.0 as freespace from sys.database_files
```

![alt](./imgs/img_15.png)

Nos va a mostrar los datos exactos del tamaño y espacio libre en cada archivo, en el de datos y en el log
